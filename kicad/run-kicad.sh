#!/bin/bash 
xhost +local:root; \
docker run -d \
--name bsim-kicad \
--rm \
--name bsim-kicad \
-e DISPLAY=$DISPLAY \
-v /tmp/.X11-unix:/tmp/.X11-unix:rw \
-v $HOME:/root:rw \
-v ~/.kicad/kicad-symbols:/usr/local/share/kicad/library:rw \
-v ~/.kicad/kicad-packages3D:/usr/share/kicad/modules/packages3d/:rw \
-v ~/.kicad/kicad-footprints:/usr/local/share/kicad/modules:rw \
dissar/kicad:latest \
kicad