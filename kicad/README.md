# Docker image for KiCad
https://kicad-pcb.org/

# Running a KiCad container

## Prerequisite: KiCad libraries
* https://kicad.github.io/symbols/
* https://kicad.github.io/footprints/
* https://kicad.github.io/packages3d/

```
git clone https://github.com/KiCad/kicad-symbols.git ~/.kicad/kicad-symbols
git clone https://github.com/KiCad/kicad-footprints.git ~/.kicad/kicad-footprints
git clone https://github.com/KiCad/kicad-packages3D.git ~/.kicad/kicad-packages3D
```

## Run container

Make the run-script executable
```
cd kicad/kicad
chmod u+x run-kicad.sh
```

Run the script 'run-kicad.sh';

```
./run-kicad.sh
```

# Building the image

## Build
Make an image with and tag it with the current KiCad version and a 'latest' tag.

```docker build -t dissar/kicad:latest -t dissar/kicad:x.x.x ./```

## Push
To push the image to dockerhub

```docker push dissar/kicad:x.x.x```