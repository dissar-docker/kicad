#!/bin/sh
xhost +local:root; \
docker run -d \
--name bsim-kicad \
--rm \
--privileged \
--name kicad-nvidia \
-e DISPLAY=$DISPLAY \
-v /tmp/.X11-unix:/tmp/.X11-unix:rw \
-v $HOME:/root:rw \
-v ~/.kicad/kicad-symbols:/usr/local/share/kicad/library:rw \
-v ~/.kicad/kicad-packages3D:/usr/share/kicad/modules/packages3d/:rw \
-v ~/.kicad/kicad-footprints:/usr/local/share/kicad/modules:rw \
kicad-nvidia \
kicad
