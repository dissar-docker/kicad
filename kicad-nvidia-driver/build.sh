#!/bin/bash

# Download NVIDIA-driver
version="$(glxinfo | grep "OpenGL version string" | rev | cut -d" " -f1 | rev)"
wget http://us.download.nvidia.com/XFree86/Linux-x86_64/"$version"/NVIDIA-Linux-x86_64-"$version".run
mv NVIDIA-Linux-x86_64-"$version".run NVIDIA-DRIVER.run

docker build -t kicad-nvidia ./

rm NVIDIA-DRIVER.run

echo "Run docker container with 'run-kicad.sh'"