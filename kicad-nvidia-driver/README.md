# Docker image for KiCad with NVIDIA-driver
https://kicad-pcb.org/

# Building the image

The Dockerfile uses dissar/kicad:latest as a base image and appends the NVIDIA-driver installed in your system. To build an image run

```./build.sh```

# Running a KiCad container

## Prerequisite: KiCad libraries
* https://kicad.github.io/symbols/
* https://kicad.github.io/footprints/
* https://kicad.github.io/packages3d/

```
git clone https://github.com/KiCad/kicad-symbols.git ~/.kicad/kicad-symbols
git clone https://github.com/KiCad/kicad-footprints.git ~/.kicad/kicad-footprints
git clone https://github.com/KiCad/kicad-packages3D.git ~/.kicad/kicad-packages3D
```

## Run container

Make the run-script executable
```
cd kicad/kicad
chmod u+x run-kicad.sh
```

Run the script 'run-kicad.sh';

```
./run-kicad.sh
```